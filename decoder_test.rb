require "minitest/autorun"
require File.join(File.dirname(__FILE__), "decoder")

describe Geohash::Decoder do

  it "should decode an expected geohash to coordinates" do
    @decoded = Geohash::Decoder.decode("ezs42")
    assert_equal [ 42.6, -5.6 ], @decoded
  end

end
